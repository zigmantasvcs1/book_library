﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class ReservationAdded : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Readers",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "BorrowRecords",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Books",
                newName: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Readers",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "BorrowRecords",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Books",
                newName: "ID");
        }
    }
}
