﻿using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public class ReaderRepository : BaseRepository<Reader>
    {
        public ReaderRepository(BooksDbContext context) : base(context)
        {
        }
    }
}
