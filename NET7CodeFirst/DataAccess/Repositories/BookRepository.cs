﻿using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public class BookRepository : BaseRepository<Book>
    {
        public BookRepository(BooksDbContext context) : base(context)
        {
        }
    }
}
