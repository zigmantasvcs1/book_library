﻿namespace DataAccess.Entities
{
    public class Reservation : BaseEntity
    {
        public int BookId { get; set; }
        public Book Book { get; set; }
        public int ReaderId { get; set; }
        public Reader Reader { get; set; }
        public DateTime ReservationDate { get; set; }
    }
}
