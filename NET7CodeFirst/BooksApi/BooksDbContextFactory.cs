﻿using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace BooksApi
{
    public class BooksDbContextFactory
    {
        public static BooksDbContext CreateDbContext()
        {
            // konfiguracijos uzkelimas is appsettings.json
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            // connection stirng pasiemimas
            string connectionString = configuration.GetConnectionString("DefaultConnection");

            var builder = new DbContextOptionsBuilder<BooksDbContext>();
            builder.UseSqlServer(connectionString);

            return new BooksDbContext(builder.Options);
        }
    }
}
