﻿using System.ComponentModel.DataAnnotations;

namespace DataAccess.Entities
{
    public class Book : BaseEntity
    {
        public string Title { get; set; }
        public string Author { get; set; }
        [Range(1450, 2024)]
        public int PublishedYear { get; set; }
        public bool IsAvailable { get; set; }
    }
}
