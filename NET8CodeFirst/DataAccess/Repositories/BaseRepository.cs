﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace DataAccess.Repositories
{
    // BaseRepository klasė skirta bendrai CRUD (Sukurti, Skaityti, Atnaujinti, Ištrinti) funkcionalumui su bet kuria entity,
    // kuri paveldi iš BaseEntity. Tai leidžia išvengti kodo pasikartojimo kuriant specifinius repositorius kiekvienai entity klasei.
    public class BaseRepository<TEntity> where TEntity : BaseEntity
    {
        private BooksDbContext _context; // Kontekstas duomenų bazės operacijoms
        private DbSet<TEntity> _entities; // Nurodoma, su kuria entity dirbama duomenų bazėje

        // Konstruktorius priima duomenų bazės kontekstą ir nustato su kokia entity dirbama
        public BaseRepository(BooksDbContext context)
        {
            _context = context;
            _entities = _context.Set<TEntity>();
        }

        // Metodas entity sąrašui gauti su galimais filtrais, include išraiškomis ir rezultatų limitu
        public async Task<List<TEntity>> ListAsync(
            Expression<Func<TEntity, bool>>? filter = null, // Pasirenkamas filtro išraiška
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null, // Pasirenkama include išraiška
            int? limit = null) // Pasirenkamas rezultatų limitas
        {
            IQueryable<TEntity> query = _entities;

            if (include != null) // Jei yra nurodyta include išraiška
            {
                query = include(query); // Taikome include išraišką užklausai
            }

            if (filter != null) // Jei yra nurodytas filtras
            {
                query = query.Where(filter); // Taikome filtrą užklausai
            }

            if (limit.HasValue) // Jei yra nurodytas limitas
            {
                query = query.Take(limit.Value); // Taikome limitą užklausai
            }

            return await query.ToListAsync(); // Vykdome užklausą asinchroniškai ir grąžiname rezultatą
        }

        // CreateAsync metodas leidžia įrašyti naują entity į duomenų bazę
        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            await _entities.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        // Metodas vienam entity gauti pagal ID su galimais include
        public async Task<TEntity?> GetAsync(
            int id,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? include = null) // Pasirenkama include išraiška
        {
            IQueryable<TEntity> query = _entities;

            if (include != null) // Jei yra nurodyta include išraiška
            {
                query = include(query); // Taikome include išraišką užklausai
            }

            return await query.FirstOrDefaultAsync(e => e.Id == id); // Vykdome užklausą, kad rastume entity pagal ID
        }

        // UpdateAsync metodas atnaujina esamą entity duomenų bazėje
        public async Task UpdateAsync(TEntity entity)
        {
            var existingEntity = await _entities.FindAsync(entity.Id); // Ieškoma entity pagal ID

            if (existingEntity == null) // Jei entity nerasta, išmetama išimtis
            {
                throw new KeyNotFoundException($"{typeof(TEntity).Name} with ID {entity.Id} not found.");
            }

            _context.Entry(existingEntity).CurrentValues.SetValues(entity); // Atnaujinamos esamos entity reikšmės

            await _context.SaveChangesAsync(); // Išsaugomi pakeitimai
        }

        // DeleteAsync metodas ištrina entity iš duomenų bazės pagal jos ID
        public async Task<bool> DeleteAsync(int id)
        {
            TEntity? entity = await GetAsync(id);

            if (entity == null) // Jei entity nerasta, grąžinamas false
            {
                return false;
            }

            _entities.Remove(entity); // Pašalinama entity iš duomenų bazės
            await _context.SaveChangesAsync();
            return true; // Grąžinamas true, nurodant, kad ištrinimas sėkmingas
        }
    }
}
