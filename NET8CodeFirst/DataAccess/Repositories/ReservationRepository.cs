﻿using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public class ReservationRepository : BaseRepository<Reservation>
    {
        public ReservationRepository(BooksDbContext context) : base(context)
        {
        }
    }
}
