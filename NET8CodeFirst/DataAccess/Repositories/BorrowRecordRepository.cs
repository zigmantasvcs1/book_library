﻿using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public class BorrowRecordRepository : BaseRepository<BorrowRecord>
    {
        public BorrowRecordRepository(BooksDbContext context) : base(context)
        {
        }
    }
}
