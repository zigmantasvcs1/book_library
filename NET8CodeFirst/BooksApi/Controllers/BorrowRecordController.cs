﻿using DataAccess.Entities;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BooksApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BorrowRecordController : ControllerBase
    {
        private readonly BorrowRecordRepository _borrowRecordRepository;
        private readonly BookRepository _bookRepository;

        public BorrowRecordController()
        {
            var context = BooksDbContextFactory.CreateDbContext();
            _borrowRecordRepository = new BorrowRecordRepository(context);
            _bookRepository = new BookRepository(context);
        }

        // GET: api/BorrowRecord/5
        [HttpGet("{readerId}")]
        public async Task<IActionResult> GetBorrowRecordsByReader(int readerId)
        {
            // TODO: Validate that the provided readerId is a valid integer and not the default value.
            // TODO: Consider adding a try-catch block to handle any potential database errors gracefully.
            var borrowRecords = await _borrowRecordRepository.ListAsync(
                br => br.ReaderId == readerId,
                include: query => query.Include(br => br.Book)
            ); 

            return Ok(borrowRecords);
        }

        // POST: api/BorrowRecord/borrow
        [HttpPost("borrow")]
        public async Task<IActionResult> BorrowBook([FromBody] BorrowRecord borrowRecord)
        {
            // TODO: Add model validation to ensure borrowRecord properties are valid according to your model definitions.
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var book = await _bookRepository.GetAsync(borrowRecord.BookId);
            if (book == null || !book.IsAvailable)
            {
                return BadRequest("Book is not available or does not exist.");
            }

            // TODO: Surround database operations with a try-catch block to handle exceptions such as DbUpdateException.
            book.IsAvailable = false; // Mark the book as borrowed
            await _bookRepository.UpdateAsync(book);

            borrowRecord.BorrowDate = DateTime.UtcNow;
            borrowRecord.ReturnDate = null; // Indicating the book is not yet returned

            await _borrowRecordRepository.CreateAsync(borrowRecord);

            return Ok(borrowRecord);
        }
    }
}
