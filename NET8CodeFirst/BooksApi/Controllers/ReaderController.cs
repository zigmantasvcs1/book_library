﻿using DataAccess.Entities;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace BooksApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReaderController : ControllerBase
    {
        private readonly ReaderRepository _readerRepository;

        public ReaderController()
        {
            var context = BooksDbContextFactory.CreateDbContext();
            _readerRepository = new ReaderRepository(context);
        }

        // GET: api/Reader
        [HttpGet]
        public async Task<IActionResult> GetAllReaders()
        {
            // TODO: Consider adding a try-catch block to handle potential exceptions when accessing the database.
            var readers = await _readerRepository.ListAsync();
            // TODO: Check if the readers list is empty and decide if you want to return a different status code or message.
            return Ok(readers);
        }

        // POST: api/Reader
        [HttpPost]
        public async Task<IActionResult> RegisterNewReader([FromBody] Reader newReader)
        {
            // TODO: Add validation for newReader properties that cannot be validated by ModelState.IsValid.
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Wrap the creation logic in a try-catch block to handle exceptions such as duplicate entries or database connectivity issues.
            newReader.RegistrationDate = DateTime.UtcNow; // Set registration date to now
            var createdReader = await _readerRepository.CreateAsync(newReader);
            // TODO: After creating the reader, verify if the creation was successful before returning the response.
            return Ok(createdReader);
        }
    }
}
