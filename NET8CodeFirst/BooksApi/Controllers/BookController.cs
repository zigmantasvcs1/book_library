﻿using DataAccess.Entities;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace BooksApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly BookRepository _bookRepository;
        private readonly ReservationRepository _reservationRepository;
        private readonly BorrowRecordRepository _borrowRecordRepository;

        public BookController()
        {
            var context = BooksDbContextFactory.CreateDbContext();
            _bookRepository = new BookRepository(context);
            _reservationRepository = new ReservationRepository(context);
            _borrowRecordRepository = new BorrowRecordRepository(context);
        }

        [HttpPost]
        public async Task<IActionResult> CreateBook([FromBody] Book newBook)
        {
            var createdBook = await _bookRepository.CreateAsync(newBook);
            // TODO: Add additional validation here to ensure the book was successfully created before returning.

            // TODO: Add a try-catch block around the database operation. In the catch block, log the exception and return an appropriate error response.
            return Ok(createdBook);
        }

        [HttpGet]
        public async Task<IActionResult> GetAvailableBooksAsync()
        {
            var books = await _bookRepository.ListAsync(x => x.IsAvailable);

            // TODO: Consider adding a check here to see if the books list is empty and decide how to handle this scenario (e.g., return a NotFound or a different response).

            // TODO: Surround the database call with a try-catch block for error handling. In the catch block, you should log the error and return a generic error message to the client, indicating a problem occurred while processing their request.
            return Ok(books);
        }

        [HttpPost("reserve/{bookId}/{readerId}")]
        public async Task<IActionResult> ReserveBook(int bookId, int readerId)
        {
            // TODO: Validate the incoming parameters (bookId and readerId) to ensure they are positive integers.
            // return BadRequest if not.
            var book = await _bookRepository.GetAsync(bookId);

            if (book == null || !book.IsAvailable)
            {
                return BadRequest("Book is not available or does not exist.");
            }

            // TODO: Add a try-catch block around the reservation logic.
            // In the catch block, log the exception and return a 500 Internal Server Error status code.
            // Explain that the catch block should handle any unexpected errors during the reservation process,
            // such as issues with database connectivity or constraints being violated.

            var reservation = new Reservation
            {
                BookId = bookId,
                ReaderId = readerId,
                ReservationDate = DateTime.UtcNow
            };

            await _reservationRepository.CreateAsync(reservation);

            book.IsAvailable = false;
            await _bookRepository.UpdateAsync(book);

            return Ok("Book reserved successfully.");
        }

        [HttpPost("return/{bookId}")]
        public async Task<IActionResult> ReturnBook(int bookId)
        {
            // TODO: Validate that the provided bookId is a valid integer and not the default value.
            var book = await _bookRepository.GetAsync(bookId);

            // TODO: validate if not null, if null return NotFound.

            var borrowRecord = (await _borrowRecordRepository.ListAsync(x => x.BookId == bookId))
                .SingleOrDefault();

            if (borrowRecord != null)
            {
                borrowRecord.ReturnDate = DateTime.UtcNow;
                // TODO: Surround the update operation with a try-catch block. Handle potential exceptions by logging the error and returning a 500 Internal Server Error status code with a generic error message.
                await _borrowRecordRepository.UpdateAsync(borrowRecord);
            }

            book.IsAvailable = true;
            await _bookRepository.UpdateAsync(book);

            return Ok("Book returned successfully.");
        }
    }
}
